/* A java class to deal with the various AI plays */

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import java.lang.*;

public class ComputerPlayer {

  static int Nim = 8;
  static Random rand = new Random();
  static int computerTurn = 1;
  static String strategy = "";
  public ComputerPlayer() {

  }
	
  //First moves to score if possible, then moves to block if possible,
  //then moves randomly
  public static int moveRandom(int a) {
    int player = a;
    if (player == 1) {
      if (Board.player2WinMove() != -1) {
        return Board.player2WinMove();
      }
      else {
        if (Board.player1WinMove() != -1) {
          return Board.player1WinMove();
        }
        else {
          int randint = rand.nextInt(Nim);
          while (!Board.checkMove(randint)) {
            randint = rand.nextInt(Nim);
          }
          return randint;
        }
      }
    }
    if (player == 2) {
      if (Board.player1WinMove() != -1) {
        return Board.player1WinMove();
      }
      else {
        if (Board.player2WinMove() != -1) {
          return Board.player2WinMove();
        }
        else {
          int randint = rand.nextInt(Nim);
          while (!Board.checkMove(randint)) {
            randint = rand.nextInt(Nim);
          }
          return randint;
        }
      }
    }
    else {
      return -10;
    }
  }
  
//  0  1  2
//  3  4  5
//  6  7  8

    public static int moveStrategic(int a) {
    int player = a;
      if (player == 2) {
        if (computerTurn == 1) {
          computerTurn++;
          int randint = rand.nextInt(4);
            if (randint == 0) {return 0;}
            if (randint == 1) {return 2;}
            if (randint == 2) {return 6;}
            if (randint == 3) {return 8;}
            else {return -1;}
        }
        if (computerTurn == 2) {
          computerTurn++;
          //If they move middle, move to the opposite corner
          if (Board.getPlayer2Moves().contains(4)) {
            strategy = "classic";
            System.out.println("classic");
            return getOppositeCorner(Board.getPlayer1Moves().get(0));
          }
          //If they move to a non-corner, move middle
          if (checkNonCorner(Board.getPlayer2Moves().get(0)) >= 0) {
            strategy = "finished";
            System.out.println("finished");
            return 4;
          }
          //If they move to the opposite corner, move to an adjacent corner
          if (Board.getPlayer1Moves().contains(getOppositeCorner(Board.getPlayer2Moves().get(0)))) {
            System.out.println("baked");
            strategy = "baked";       
            return getRandomAdjCorner(Board.getPlayer1Moves().get(0));
          }
          //If they move to an adjacent corner, move to the computer's opposite corner
          else {
            strategy = "scooped";
            System.out.println("scooped");
            return getOppositeCorner(Board.getPlayer1Moves().get(0));
          }
        }
        if (computerTurn == 3) {
          computerTurn++;
          if (Board.player1WinMove() != -1) {
            return Board.player1WinMove();
          }
          if (Board.player2WinMove() != -1) {
            return Board.player2WinMove();
          }
          else {
            if (strategy.equals("classic")) {
              return getRandomAdjCorner(Board.getPlayer1Moves().get(0));
            }
            if (strategy.equals("finished")) {
              return getCorrectCorner();
            }
            //Return unoccupied corner
            if (strategy.equals("baked")) {
              if (Board.getGameBoard()[0] == ' '){
                return 0;
              }
              if (Board.getGameBoard()[2] == ' '){
                return 2;
              }
              if (Board.getGameBoard()[6] == ' '){
                return 6;
              }
              if (Board.getGameBoard()[8] == ' '){
                return 8;
              }
            else {return -100;}
            }
            if (strategy.equals("scooped")) {
              return moveRandom(player);
            }
          return -1;
          }
        }
        if (computerTurn > 3) {
          computerTurn++;
          return moveRandom(player);
        }
        return -2;
      }
      if (player == 1) {
        if (computerTurn == 1) {
          computerTurn++;
          if (Board.getPlayer1Moves().get(0) == 4) {
            strategy = "middle";
            return 2;
          }
          if (checkNonCorner(Board.getPlayer1Moves().get(0)) < 0) {
            strategy = "corner";
            return 4;
          }
          return moveRandom(player);
        }
        if (computerTurn == 2) {
          computerTurn++;
          if (Board.player1WinMove() != -1) {
            return Board.player1WinMove();
          }
          else {               
            if (strategy.equals("middle")) {
              return 0;
            }
            if (strategy.equals("corner")) {
              if (Board.getPlayer1Moves().contains(1)){
                return 3;
              }
              else {
                return 1;
              }
            }
            else {
              return moveRandom(1);
            }          
          }
//        return moveRandom(1);
        }
      return moveRandom(1);
      }
    return -3;
  }

  public static int getOppositeCorner (int a) {
    int init = a;
    if (init == 0) {return 8;}
    if (init == 8) {return 0;}
    if (init == 6) {return 2;}
    if (init == 2) {return 6;}
    else {return -4;}
  }
  
  //If a non-corner edge, returns the nonCorner. Otherwise, returns -5
  public static int checkNonCorner (int a) {
    int init = a;
    if (init == 3) {return 3;}
    if (init == 1) {return 1;}
    if (init == 5) {return 5;}
    if (init == 7) {return 7;}
    else {return -5;}
  }
  
  //Returns a random adjacent corner
  public static int getRandomAdjCorner (int a) {
    int init = a;
    int randint = rand.nextInt(1);
    if (randint == 0) {
      if (init == 0) {return 2;}
      if (init == 8) {return 6;}
      if (init == 6) {return 0;}
      if (init == 2) {return 8;}
    }
    if (randint == 1) {
      if (init == 0) {return 6;}
      if (init == 8) {return 2;}
      if (init == 6) {return 8;}
      if (init == 2) {return 0;}
    }
    return -6;
  }

//Function is for move 3 in finished (player two's first move is corner)
//If the player2's second move is a side placement, move to the correct
//Corner
// X| |*
// O|X|
//  | |O

  //Specific to move 2 of strategy Finished  
  public static int getCorrectCorner () {
    int[][] moveIndex = {
      {3,0,2},
      {1,2,5},
      {5,8,6},
      {7,6,0},
      {3,6,8},
      {1,0,6},
      {5,2,0},
      {7,8,2}
    };
    for (int i = 0; i < 8; i++) {
      if(Board.getPlayer2Moves().contains(moveIndex[i][0]) && Board.getPlayer1Moves().contains(moveIndex[i][1])){
        return moveIndex[i][2];
      }
    }
    return -7;
  }
        
}
