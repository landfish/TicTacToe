/* A java class to handle displaying my tictactoe board */

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class DisplayBoard extends JPanel implements MouseListener {

  public static final int[][] moveSpaces = {
    {100,100},
    {300,100},
    {500,100},
    {100,300},
    {300,300},
    {500,300},
    {100,500},
    {300,500},
    {500,500}
  };

  private static char[] boardPositions;
  public static int xcord;
  public static int ycord;

  public DisplayBoard () {
    setPreferredSize(new Dimension(1200, 600));
    addMouseListener(this);
  }

  public void showBoard() {
     JFrame frame = new JFrame("Game Board");
     Container cnt = frame.getContentPane();
     DisplayBoard pnl = new DisplayBoard();
     cnt.add(pnl);
     frame.pack();
     frame.setVisible(true);
     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


   public void paintComponent(Graphics page) {
    boardPositions = Board.getGameBoard();
    super.paintComponent(page);
    page.drawLine(200,0, 200, 600);
    page.drawLine(400,0, 400, 600);
    page.drawLine(0,200, 600, 200);
    page.drawLine(0,400, 600, 400);
    drawSymbols(page,boardPositions);
  }

  public void refresh() {
    repaint();
  }

  public static void drawX (Graphics page, int x, int y) {
    int cordX = x;
    int cordY = y;
    page.setColor(Color.black);
    page.drawLine(cordX,cordY,cordX + 50,cordY + 100);
    page.drawLine(cordX + 50,cordY,cordX,cordY + 100);
  }

  public static void drawSymbols(Graphics page, char[] a) {
    char[] symbols = a;
    for (int i = 0; i < 9; i++) {
      if (symbols[i] == 'X'){
        drawX(page,moveSpaces[i][0]-25,moveSpaces[i][1]-50);
      }
      if (symbols[i] == 'O'){
        drawO(page,moveSpaces[i][0]-25,moveSpaces[i][1]-50);
      }
    }
  }
    

  public static void drawO (Graphics page, int x, int y) {
    int cordX = x;
    int cordY = y;
    page.setColor(Color.red);
    page.drawOval(cordX,cordY,75,100);
  }

  // MouseListener events
  public void mousePressed(MouseEvent e) {
    xcord = e.getX();
    ycord = e.getY(); 
    boardPositions = Board.getGameBoard();
    GameDriver.toggleClickTrue();
    repaint();
  }

  public void mouseReleased(MouseEvent e) {}
  public void mouseEntered(MouseEvent e) {}
  public void mouseExited(MouseEvent e) {}
  public void mouseClicked(MouseEvent e) {}

  public static int getXCord() {
    return xcord;
  }

  public static int getYCord() {
    return ycord;
  }

}
