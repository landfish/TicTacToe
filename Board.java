import java.util.*;

public class Board {

  //Initial arrays and variables 
  public static char[] gameBoard;
  public static ArrayList<Integer> player1Moves;
  public static ArrayList<Integer> player2Moves;
  public static String playerWin;
  public static final int[][] WINS = {
      {0,3,6},
      {1,4,7},
      {2,5,8},
      {0,1,2},
      {3,4,5},
      {6,7,8},
      {0,4,8},
      {6,4,2}
    };

  //Constructor initates blank board
  public Board (int a) {
    //firstPlayer variable doesn't yet do anything
    int firstPlayer = a;
    gameBoard = new char[9];
    for (int i = 0; i < 9; i++){
      gameBoard[i] = ' ';
    }
    player1Moves = new ArrayList<Integer>();
    player2Moves = new ArrayList<Integer>();
  }


  // METHODS

  //Returns the tic-tac-toe board with the current moves
  public static String printBoard(){
    String tempBoard = "";
    tempBoard = gameBoard[0] + "|" + gameBoard[1] + "|" + gameBoard[2] + "\n" + gameBoard[3] + "|" + gameBoard[4] + "|" + gameBoard[5] + "\n" + gameBoard[6] + "|" + gameBoard[7] + "|" + gameBoard[8];
    return tempBoard;
  }  

  //Returns the numbered positions of the board to be used by players as a reference
  public static String printPositions(){
    String tempBoard = "";
    tempBoard = 0 + "|" + 1 + "|" + 2 + "\n" + 3 + "|" + 4 + "|" + 5 + "\n" + 6 + "|" + 7 + "|" + 8;
    return tempBoard;
  }

  //Returns both the reference positions and the current moves, side by side
  public static String printBoardPositions(){
    String tempBoard = "";
    tempBoard = gameBoard[0] + "|" + gameBoard[1] + "|" + gameBoard[2] + "       " + 0 + "|" + 1 + "|" + 2 + "\n" + gameBoard[3] + "|" + gameBoard[4] + "|" +  gameBoard[5] + "       " + 3 + "|" + 4 + "|" + 5 + "\n" + gameBoard[6] + "|" + gameBoard[7] + "|" + gameBoard[8] + "       " + 6 + "|" + 7 + "|" + 8;
    return tempBoard;
  }

  public static boolean checkMove(int a){
    if (gameBoard[a] == ' '){
      return true;
    }
    else {
      return false;
    }
  }

  public static void player1Move(int a) {
    gameBoard[a] = 'X';
    player1Moves.add(a);
  }

  public static void player2Move(int a) {
    gameBoard[a] = 'O';
    player2Moves.add(a);
  }

  //Checks to see if either player has three letters in a line
  public static boolean checkWin() {
    int threeCount;
    for (int i = 0; i < 8; i++) {
      threeCount = 0;
      for (int a = 0; a < 3; a++) {
        if (player1Moves.contains(WINS[i][a])) {
          threeCount++;
        }
      }
      if (threeCount == 3) {
        playerWin = GameDriver.getPlayer1();
        return true;
      }
      threeCount = 0;
      for (int a = 0; a < 3; a++) {
        if (player2Moves.contains(WINS[i][a])) {
          threeCount++;
        }
      }
      if (threeCount == 3) {
        playerWin = GameDriver.getPlayer2();
        return true;
      }
    }
    return false;
  }    
  
  //Returns true only if no positions are blank
  public static boolean checkFilled() {
    for (int i = 0; i < 9; i++) {
      if (gameBoard[i] == ' ') {
        return false;
      }
    }
    return true;
  }


  //For player 1: Returns the number of the winning move if it exists, and -1 otherwise
  public static int player1WinMove() {
    int twoCount;
    int freeMove;
    for (int x = 0; x < 8; x++) {
      freeMove = -1;
      twoCount = 0;
      for (int a = 0; a < 3; a++) {
        for (int i = 0; i < player1Moves.size(); i++) {
          if (player1Moves.get(i).equals((WINS[x][a]))) {
            twoCount++;
          }
          if(checkMove(WINS[x][a])) {
            freeMove = WINS[x][a];
          }
          if (twoCount == 2 && freeMove != -1) {
            return freeMove;
          }
        }
      }    
    } 
    return -1;
  }   

  //For player 2: Returns the number of the winning move if it exists, and -1 otherwise
  public static int player2WinMove() {
    int twoCount;
    int freeMove;
    for (int x = 0; x < 8; x++) {
      freeMove = -1;
      twoCount = 0;
      for (int a = 0; a < 3; a++) {
        for (int i = 0; i < player2Moves.size(); i++) {
          if (player2Moves.get(i).equals((WINS[x][a]))) {
            twoCount++;
          }
          if(checkMove(WINS[x][a])) {
            freeMove = WINS[x][a];
          }
          if (twoCount == 2 && freeMove != -1) {
            return freeMove;
          }
        }
      }    
    } 
    return -1;
  }   

  public static String getWinner() {
    return playerWin;
  }
  
  public static char[] getGameBoard() {
    char[] characterBoard = gameBoard;
    return characterBoard;
  }

  public static ArrayList<Integer> getPlayer1Moves() {
    return player1Moves;
  }

  public static ArrayList<Integer> getPlayer2Moves() {
    return player2Moves;
  }
  
}
