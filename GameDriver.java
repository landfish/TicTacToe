/** A one class tic-tac-toe game, made by Jeffrey Ladish 
**
** To-do List
** [ ] Fix error when clicking in JPanel but outside the tictactoe board
** [ ] Add pop up - 'you won/lost' box when a player or computer wins
*/



import java.io.*;
import java.util.*;
import java.util.Scanner;

public class GameDriver {

  public static char[] staticBoard;
  public static boolean clickTrue;
  public static int player1Pos;
  public static int player2Pos;
  public static int playerSelect = -1;
  public static int playFirst = -1;
  public static String welcomeMessage = "\nWelcome to tic-tac-toe. Would you like to play against the computer or against another player? \n \nPlease type:\n1 2-player Human game \n2 Easy computer game\n3 Difficult computer game (Warning, winning is impossible)";
  public static String computerMessage = "You've chosen to battle the computer. Press 1 and hit enter to go first. Press 2 and hit enter to go second.";
  public static String player1Name = "player 1";
  public static String player2Name = "player 2";
  public static void main(String[] args) {

    //Allows  user to select 2-player or 1-player vs. computer (to be implemented later)
    Scanner scan = new Scanner(System.in);
    System.out.println(welcomeMessage);
    int playerSelect = scan.nextInt();
    if (playerSelect == 2 || playerSelect == 3) {
      System.out.println(computerMessage);
      playFirst = scan.nextInt();
    }
      
    //New board is created for this game.
    Board board = new Board(playerSelect);

    DisplayBoard graphicsBoard = new DisplayBoard();
    staticBoard = Board.getGameBoard();
    graphicsBoard.showBoard();
    int player1Pos = -1;
    int player2Pos = -1;
    //Players are prompted to fill in the board during their turn until someone wins or there are no longer places to go
    while (!Board.checkWin() && !Board.checkFilled()) {
      //Player 1 is prompted to click on position
      if (playerSelect == 1) {
        player1Turn(graphicsBoard);
        //The second checkWin and checkFilled functions are necessary inside the while loop, in case the first player wins during their turn
        if (!Board.checkWin() && !Board.checkFilled()) {
          player2Turn(graphicsBoard);   
        }   
      }
      //Easy Computer Move
      if (playerSelect == 2) {
        if (playFirst == 1) {
          player1Name = "Human Player";
          player2Name = "The Computer";
          player1Turn(graphicsBoard);
          if (!Board.checkWin() && !Board.checkFilled()) {
            player2Pos = ComputerPlayer.moveRandom(2);
            Board.player2Move(player2Pos);
          }
        }
        if (playFirst == 2) {
          player1Name = "The Computer";
          player2Name = "Human Player";
          player1Pos = ComputerPlayer.moveRandom(1);
          Board.player1Move(player1Pos);
          if (!Board.checkWin() && !Board.checkFilled()) {
            player2Turn(graphicsBoard);   
          }
        }
      }
      //Hard Computer Mode
      if (playerSelect == 3) {
        if (playFirst == 1) {
          player1Name = "Human Player";
          player2Name = "The Computer";
          player1Turn(graphicsBoard);
          if (!Board.checkWin() && !Board.checkFilled()) {
            player2Pos = ComputerPlayer.moveStrategic(1);
            Board.player2Move(player2Pos);
          }
        }
        if (playFirst == 2) {
          player1Name = "The Computer";
          player2Name = "Human Player";
          player1Pos = ComputerPlayer.moveStrategic(2);
          Board.player1Move(player1Pos);
          if (!Board.checkWin() && !Board.checkFilled()) {
            player2Turn(graphicsBoard);   
          }
        }         
      }
    }
    
    //Prints out the final result of the game
    if (Board.checkFilled() && !Board.checkWin()) {
      System.out.println("Cat game! No one wins, no one loses. Except the cat. The cat wins. ^.^ ");
    }
    else {
      System.out.println("\n" + Board.getWinner() + " wins!");
    }
    //Shows the players the final board without the reference board
    System.out.println("\n" + Board.printBoard());

    }  

  public static int clickPos(DisplayBoard a) {
    if (DisplayBoard.getXCord() < 200) {
      if (DisplayBoard.getYCord() < 200) {
        return 0;
      }
      if (DisplayBoard.getYCord() < 400) {
        return 3;
      }
      if (DisplayBoard.getYCord() < 600) {
        return 6;
      }
      else {
        return -1;
      }
    }
    if (DisplayBoard.getXCord() < 400) {
      if (DisplayBoard.getYCord() < 200) {
        return 1;
      }
      if (DisplayBoard.getYCord() < 400) {
        return 4;
      }
      if (DisplayBoard.getYCord() < 600) {
        return 7;
      }
      else {
        return -1;
      }
    }
    if (DisplayBoard.getXCord() < 600) {
      if (DisplayBoard.getYCord() < 200) {
        return 2;
      }
      if (DisplayBoard.getYCord() < 400) {
        return 5;
      }
      if (DisplayBoard.getYCord() < 600) {
        return 8;
      }
      else {
        return -1;
      }
    }
    else {
      return -1;
    }
  }

  public static void printPlayer1Win() {
    if (Board.player1WinMove() == -1) {
      System.out.println("You cannot win this turn");
    }
    else {
      System.out.println("The winning move for this turn is space " + Board.player1WinMove());
    }
  }

  public static void player1Turn(DisplayBoard a) {
    DisplayBoard disboard = a;
    System.out.println("Player 1, please click on where would you like to move");
    clickTrue = false;
    while (clickTrue == false) {
      }
    player1Pos = clickPos(disboard);

    //Checks to see if the positions is free
    while (!Board.checkMove(player1Pos)) {
      System.out.println("You cannot move there, please select the number of a space that is not occupied");
      clickTrue = false;
      while (clickTrue == false) {
      }
      player1Pos = clickPos(disboard);
    }
    Board.player1Move(player1Pos);
    System.out.println("\n" + Board.printBoard());   
  }

  public static void player2Turn(DisplayBoard a) {
    DisplayBoard disboard = a;
    System.out.println("Player 2, please click on where you would like to move");
    clickTrue = false;
    while (clickTrue == false) {
    }
    player2Pos = clickPos(disboard);
    //Checks to see if the position is free
    while (!Board.checkMove(player2Pos)) {
      System.out.println("You cannot move there, please select the number of a space that is not occupied");
      clickTrue = false;
      while (clickTrue == false) {
      }
      player2Pos = clickPos(disboard);
    }
    Board.player2Move(player2Pos);
    System.out.println("\n" + Board.printBoard());
  }

  public static void toggleClickTrue() {
    clickTrue = true;
  }

  public static String getPlayer1() {
    return player1Name;
  }

  public static String getPlayer2() {
    return player2Name;
  }

}
