/* A test program to see how arrays work */

public class TestArray {

  public static void main(String[] args) {

    int test[][] = {
      {0,3,6},
      {1,4,7},
      {2,5,8},
      {0,1,2},
      {3,4,5},
      {6,7,8},
      {0,4,8}
    };

   System.out.println("testing position 0,0: " + test[0][0]);
   System.out.println("testing position 1,0: " + test[1][0]);
   System.out.println("testing position 0,1: " + test[0][1]);

  }
}
