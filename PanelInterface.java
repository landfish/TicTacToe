/* This class controls the buttons to the right of the tictactoe board */

public class PanelInterface extends JPanel implements ActionListener {

  public JPanel buttonPanel = new JPanel(); //make the panel for the buttons

  public JButton newGame = new JButton("New Game");
  public JButton quitButton = new JButton("Quit");

  public PanelInterface() {
      super();

      // this essentially sets up the whole view,
      // by adding components to "this" - a JPanel.
      // The JFrame is set up later.

      // Add an airmap
      funGame = new DisplayBoard();                                // create map with width 800, height 500, padding 30
      funGame.setAlignmentX(Component.LEFT_ALIGNMENT);                   // align to left
      funGame.setBorder(BorderFactory.createTitledBorder("Tic-Tac-Toe"));   // give it a named border
      this.add(funMap);
  }

}


